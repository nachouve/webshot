# thumbnail destination is where the thumbnail files will be moved to for serving
THUMBNAIL_DESTINATION = "static/thumbnails"

# thumbnail web directory is where the thumbnail files will be served from 
# this directory must be being served by flask or another webs server
THUMBNAIL_WEB_DIRECTORY = "static/thumbnails"

# localhost alias is the url of the server that webshot is running on.  Webshot
# can't take pictures of it's own server, so this is the workaround.  Anything here
# will be replaced with localhost in any url the server recieves
LOCALHOST_ALIAS = "http://mywebshotserver.com"

# localhost replacement will only work on port 80, not on ANY OTHER PORTS.
# if you have your web server running on a port other than 80, please use
# this flag to keep from crashing webshot when it tries to take a picture
# of itself
NO_LOCALHOST_SHOTS = False
