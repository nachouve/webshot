import subprocess
from optparse import OptionParser
from flask import Flask, request, render_template
import hashlib
import time
import conf.config as config
import re

app = Flask(__name__)
app.config.from_object(config)

def make_website_thumbnail(url, size, shrink):
    md5 = hashlib.md5()
    md5.update(str(time.time()))
    name = md5.hexdigest() + ".png"

    subprocess.Popen(['phantomjs', 'rasterize.js', url,  name]).wait()

    if shrink:
        subprocess.Popen(['convert', name, '-filter', 'Lanczos', '-thumbnail', size, name]).wait()

    subprocess.Popen(['mv', name, app.config['THUMBNAIL_DESTINATION']]).wait()
    return "<img src = '%s/%s'>" % (app.config['THUMBNAIL_WEB_DIRECTORY'], name)

def valid_or_none(dimension):
    if not dimension:
        return True

    if int(dimension) <= 0 or int(dimension) > 999:
        return False
    return True

def make_size(width, height):
    if not width:
        width = ""
    if not height:
        height = ""

    return "%sx%s" % (width, height)

def none_or_given(arg, request):
    if arg not in request:
        return None
    return request[arg]

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/thumb")
def webshot():
    if not 'url' in request.args:
        return "Please provide a url"
    url = request.args['url']

    if not (url.startswith("http://") or url.startswith("https://")):
        url = "http://" + url

    if re.match(r'https?://.*?:[0-9]{2,5}.*', url):
        return "webshot.py cannot take pictures of ports other than 80"

    if 'LOCALHOST_ALIAS' in app.config and app.config['LOCALHOST_ALIAS'] in url:
        if 'NO_LOCALHOST_SHOTS' in app.config and app.config['NO_LOCALHOST_SHOTS']:
            return "webshot.py is not configure to take screenshots of itself"
        else:
            url = url.replace(app.config['LOCALHOST_ALIAS'], 'http://localhost')

    width = none_or_given('width', request.args)
    height = none_or_given('height', request.args)

    if not width and not height:
        return make_website_thumbnail(url, "", False)

    try:
        if not valid_or_none(width) or not valid_or_none(height):
            return "Invalid width or height arguments, must be 0 < dimension <= 999"

        size = make_size(width, height)
        return make_website_thumbnail(url, size, True)
    except ValueError, e:
        return "Please provide an integer width/height: %s" % str(e)


if __name__ == "__main__":
    app.run(host = "0.0.0.0", port = 8082)
