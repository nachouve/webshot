#Webshot.py

webshot.py is a portable flask server that allows you to create thumbnails of a
site given the following dependencies:

* [Python](http://python.org) and [Flask](http://flask.pocoo.org/)
* [PhantomJS](http://phantomjs.org/)
* [ImageMagick](http://www.imagemagick.org/script/index.php)

**Note**: rasterize.js, the utility that takes the screenshots, was not written
by me.  It was borrowed from [Fredric
Cambus](http://www.cambus.net/creating-thumbnails-using-phantomjs-and-imagemagick/).

In order to run this utility, just clone the git repo, create a [virtual
environment](http://pypi.python.org/pypi/virtualenv/), install dependencies with
`pip install -r requirements.txt` and then do `python webshot.py`.  You will
have a server running on port 8082 of your website which you will be able to
navigate to and start using.

## Configuration

You should also create a configuration file in `conf\config.py` with the
following variables set:

   THUMBNAIL_DESTINATION
   THUMBNAIL_WEB_DIRECTORY
   LOCALHOST_ALIAS
   NO_LOCALHOST_SHOTS

Thumbnail destination and web directory do what you would expect, point to where
thumbnails should be stored and where they should be served out of.  Localhost
aliases is the url of the server that you are running webshot at.  For instance,
if you are running webshot on a server with the url
`http://mywebshotserver.com`, that's what would go in the `LOCALHOST_ALIAS`
variable.  Finally, webshot ocassionally has trouble generating screenshots of
itself.  If you are experiencing this problem (try taking a screenshot of the url
webshot is at), set this variable to `False`.

An example configuration file is provided with more information.
