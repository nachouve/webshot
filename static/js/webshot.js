$(function() {
    $("#submit").click(function() {
        $("#img").html("<img src = '/static/img/loading.gif' />")

        url = $("#url").val();
        width = $("#width").val();
        height = $("#height").val();
        
        $.get('/thumb', {"url":url, "width":width, "height":height}, function(data) {
            $("#img").html(data);
        });
    });
});
